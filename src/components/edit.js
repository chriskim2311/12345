import Profile from './profile';
import React from 'react';

export default () => (
  <Profile
    title="Edit profile"
      name="Dylan Foster"
      phone="1231231234"
      email= "dylan@guidebook.com"
      gender= "male"
  />
);
